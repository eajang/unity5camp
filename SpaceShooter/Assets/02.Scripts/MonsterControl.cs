﻿using UnityEngine;
using System.Collections;

public class MonsterControl : MonoBehaviour {

	public static string TAG_PLAYER = "PLAYER";
	public static string TAG_BULLET = "BULLET";
	public enum State
	{
		idle,
		trace,
		attack,
		die
	}

	public State state = State.idle;
	public float traceDist = 10.0f;
	public float attackDist = 2.0f;

	public Transform monsterTr;
	public Transform playerTr;

	public bool isDie = false;
	public GameObject bloodEffect;

	public Animator anim;
	public NavMeshAgent nvAgent;

	private int hp = 100;

	// Use this for initialization
	void Start () {
		monsterTr = GetComponent<Transform> ();
		playerTr = GameObject.FindGameObjectWithTag (TAG_PLAYER).GetComponent<Transform> ();
		anim = GetComponent<Animator> ();
		nvAgent = GetComponent<NavMeshAgent> ();
		bloodEffect = Resources.Load("BloodEffect") as GameObject;

		StartCoroutine (this.CheckMonsterState());
		StartCoroutine (this.MonsterAction ());
	}

	IEnumerator CheckMonsterState()
	{
		while (!isDie)
		{
			yield return new WaitForSeconds (0.3f);	// interval b/w two logic
			float dist = Vector3.Distance(monsterTr.position, playerTr.position);
			if (dist <= attackDist) {
				state = State.attack;
			} else if (dist <= traceDist) {
				state = State.trace;
			} else {
				state = State.idle;
			}
		}	
	}

	IEnumerator MonsterAction()
	{
		while (!isDie) 
		{
			yield return new WaitForSeconds (0.1f);
			switch (state) {
			case State.idle:
				anim.SetBool ("IsTrace", false);
				nvAgent.Stop ();
				break;
			case State.trace:
				anim.SetBool ("IsTrace", true);
				anim.SetBool ("IsAttack", false);
				nvAgent.SetDestination (playerTr.position);
				nvAgent.Resume ();
				break;
			case State.attack:
				anim.SetBool ("IsAttack", true);
				nvAgent.Stop ();
				break;
			case State.die:
				break;
			}
		}
	}

	void OnCollisionEnter(Collision coll)
	{
		if (coll.gameObject.tag == TAG_BULLET) {
			Instantiate (bloodEffect, coll.transform.position, Quaternion.identity);
			Destroy (coll.gameObject);
			anim.SetTrigger ("Hit");
			hp -= 25;
			if (hp <= 0) {
				MonsterDie ();
			}
		}
	}

	void MonsterDie()
	{
		StopAllCoroutines ();
		anim.SetTrigger ("Die");
		GetComponent<CapsuleCollider> ().enabled = false;
		nvAgent.Stop ();
	}
}
