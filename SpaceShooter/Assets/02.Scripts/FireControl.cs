﻿using UnityEngine;
using System.Collections;

public class FireControl : MonoBehaviour {

	public GameObject bullet;
	public Transform firePosition;
	public AudioClip fireSfx;

	private AudioSource _audio;


	void Start() {
		_audio = GetComponent<AudioSource> ();
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {	// left-mousebutton click
			Instantiate(bullet, firePosition.position, firePosition.rotation);	// dynamic create
			_audio.PlayOneShot(fireSfx, 1.0f);
		}
	}
}
