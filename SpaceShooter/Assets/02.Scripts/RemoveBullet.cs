﻿using UnityEngine;
using System.Collections;

public class RemoveBullet : MonoBehaviour {

	private static string TAG_BULLET = "BULLET";
	public GameObject sparkEffect;

	void OnCollisionEnter(Collision coll)
	{
		if (coll.gameObject.tag == TAG_BULLET) {
			GameObject dynamicObject = (GameObject) Instantiate (sparkEffect, coll.transform.position, Quaternion.identity);
			Destroy (coll.gameObject);
			Destroy (dynamicObject.gameObject, 0.5f);
		}
	}
}
