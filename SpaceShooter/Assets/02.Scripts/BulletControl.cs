﻿using UnityEngine;
using System.Collections;

public class BulletControl : MonoBehaviour {

	public Rigidbody rbody;

	// Use this for initialization
	void Start () {
		rbody = GetComponent<Rigidbody> ();
		rbody.AddForce (transform.forward * 1000.0f);
	}

}
