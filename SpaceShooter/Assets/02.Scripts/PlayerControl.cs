﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Anims 
{
	public AnimationClip idle;
	public AnimationClip runForward;
	public AnimationClip runBackward;
	public AnimationClip runRight;
	public AnimationClip runLeft;
	public AnimationClip[] die;			// can randomize die-animation
}
public class PlayerControl : MonoBehaviour {
	
	public float moveSpeed = 8.0f;
	public float rotationSpeed = 200.0f;
	private Transform tr;

	public Anims anims;
	[HideInInspector]
	public Animation _animation;

	// Use this for initialization
	void Start () {
		tr = GetComponent<Transform>();
		_animation = GetComponent<Animation>();

		_animation.clip = anims.idle;
		_animation.Play();
	}
	
	// Update is called once per frame
	void Update () {
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");


		Vector3 movDir = (Vector3.forward * v) + (Vector3.right * h);
		tr.Translate (movDir.normalized * moveSpeed * Time.deltaTime);		// normalized: vector normalized
		tr.Rotate(Vector3.up * rotationSpeed * Input.GetAxis("Mouse X") * Time.deltaTime);

		if (v >= 0.1f) {
			_animation.CrossFade (anims.runForward.name, 0.3f);
		} else if (v <= -0.1f) {
			_animation.CrossFade (anims.runBackward.name, 0.3f);
		} else if (h >= 0.1f) {
			_animation.CrossFade (anims.runRight.name, 0.2f);
		} else if (h <= -0.1f) {
			_animation.CrossFade (anims.runLeft.name, 0.2f);
		} else {
			_animation.CrossFade (anims.idle.name, 0.3f);
		}


		/*	Vector3.forward = Vector3(0, 0, 1);
			Vector3.right 	= Vector3(1, 0, 0);
			Vector3.up 		= Vector3(0, 1, 0);
			Vector3.zero	= Vector3(0, 0, 0);
			Vector3.one		= Vector3 (1, 1, 1);*/
	}
}
