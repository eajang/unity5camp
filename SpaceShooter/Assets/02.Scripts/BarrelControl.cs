﻿using UnityEngine;
using System.Collections;

public class BarrelControl : MonoBehaviour {
	public int hitCount = 0;
	private static string TAG_BULLET = "BULLET";
	public Texture[] textures;

	void Start() 
	{
		int idx = Random.Range (0, 2);
		GetComponentInChildren<MeshRenderer> ().material.mainTexture = textures[idx];
	}

	void OnCollisionEnter(Collision coll) 
	{
		if (coll.gameObject.tag == TAG_BULLET) {
			Destroy (coll.gameObject);
			if (++hitCount >= 3) {
				ExpBarrel ();
			}
		}
	}

	void ExpBarrel()
	{
		Rigidbody rbody = this.gameObject.AddComponent<Rigidbody> ();
		if (rbody != null)
			rbody.AddForce (Vector3.up * 1000.0f);
		Destroy (this.gameObject, 2.0f);
	}
}

